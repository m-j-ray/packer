# packer

build images with packer

## Submodules

- **Adding kickstart/preseed**

    ```
    git clone -b main --single-branch --depth=1 https://pagure.io/fedora-kickstarts.git http/fedora/rawhide
    git submodule add -b main https://pagure.io/fedora-kickstarts.git http/fedora/rawhide
    ```

## Scripted Installs

**Fedora/RHEL/Centos**

- https://access.redhat.com/labs/kickstartconfig/
- https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_installation/kickstart-commands-and-options-reference_installing-rhel-as-an-experienced-user#include_kickstart-commands-for-installation-program-configuration-and-flow-control
- https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_installation/kickstart-commands-and-options-reference_installing-rhel-as-an-experienced-user#cdrom_kickstart-commands-for-installation-program-configuration-and-flow-control
- https://docs.fedoraproject.org/en-US/fedora/rawhide/install-guide/advanced/Kickstart_Installations/
- https://docs.fedoraproject.org/en-US/fedora/rawhide/install-guide/advanced/Boot_Options/

**Ubuntu Autoinstall**

- https://ubuntu.com/server/docs/install/autoinstall
- https://ubuntu.com/server/docs/install/autoinstall-quickstart
- https://ubuntu.com/server/docs/install/autoinstall-reference
- https://snapcraft.io/autoinstall-generator    (preseed converter)

