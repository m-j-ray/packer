Inventory
==============

The Packer Ansible provisioner will automatically generate an inventory file during a build. Specifiy this directory using the `inventory_directory` parameter in order to take advantage of Ansible's `host_vars` and `group_vars` functionality.

- Source vars from: `./ansible/inventory/group_vars/all/*`

    ```
    build {
    sources = ["source.linode.example"]

    provisioner "ansible" {
        inventory_directory     = "./ansible/inventory"
        playbook_file           = "./ansible/podman.yml"
        use_proxy               = false
    }
    ```

- Source vars from: `./ansible/inventory/group_vars/{group1,group2}/*`

    ```
    build {
    sources = ["source.linode.example"]

    provisioner "ansible" {
        groups                  = ["group1,group2"]
        inventory_directory     = "./ansible/inventory"
        playbook_file           = "./ansible/podman.yml"
        use_proxy               = false
    }
    ```

- Source vars from: `./ansible/inventory/host_vars/web01/*`

    ```
    build {
    sources = ["source.linode.example"]

    provisioner "ansible" {
        groups                  = ["group1,group2"]
        host_alias              = "web01"
        inventory_directory     = "./ansible/inventory"
        playbook_file           = "./ansible/podman.yml"
        use_proxy               = false
    }
    ```
