Group Variables
===============

Create a variables file for each group in this folder as needed. 
Alternatively, create a subfolder of variable files for each group.
Symlinks are also supported

By default, Ansible will apply all variables contained in a file or folder to a group with a matching name.

Refer to [Ansible Variable Precedence](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable) for more information.