Global Variables
================

Variable files that apply to all hosts/groups should be placed in this folder.
This is useful for items that are common across all Packer images.
