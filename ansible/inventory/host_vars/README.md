Host Variables
==============

Create a variables file for each host in this folder as needed. 
Alternatively, create a subfolder of variable files for each host.
Symlinks are also supported

By default, Ansible will apply all variables contained in a file or folder to a host with a matching name.

You should strive to create group_vars rather than host_vars when the variables are not specific to a host.

Refer to [Ansible Variable Precedence](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable) for more information.