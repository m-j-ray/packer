Roles-local
==============

This directory is for the development of roles and will **not** be ignored by git.

Create an Ansible role skel:

```
cd ansible/roles-local
ansible-galaxy role init role_name
```
