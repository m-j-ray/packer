lang en_US
keyboard us
timezone America/New_York --isUtc
rootpw $2b$10$cISKA6irygtvG/a9QmwGG.9FlYQe4JTEWYC/nNVnyPdbehEFvCqNq --iscrypted
#platform x86, AMD64, or Intel EM64T
text
cdrom
part biosboot --fstype=biosboot --size=1
bootloader --location=mbr --append="rhgb quiet crashkernel=auto"
zerombr
clearpart --all --initlabel
volgroup vg0 --pesize=4096 pv.0 
part pv.0 --fstype=lvmpv --ondisk=sda --size=10240
logvol /boot --vgname=vg0 --name=lv_boot --fstype=xfs --size=1024
logvol / --vgname=vg0 --name=lv_root --fstype=xfs --size=1024
logvol /var --vgname=vg0 --name=lv_var --fstype=xfs --size=1024
logvol /var/log --vgname=vg0 --name=lv_var_log --fstype=xfs --size=1024
logvol /var/log/audit --vgname=vg0 --name=lv_var_log_audit --fstype=xfs --size=1024
logvol /home --vgname=vg0 --name=lv_home --fstype=xfs --size=1024
logvol swap --vgname=vg0 --name=lv_swap --fstype=swap --recommended
auth --passalgo=sha512 --useshadow
selinux --enforcing
firewall --enabled --http --ssh
firstboot --disable
%packages
@^minimal-environment
kexec-tools
%end
