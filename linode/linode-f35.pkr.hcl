variable "linode_token" {
  type    = string
  default = ""
}

variable "sshkeys" {
  type = list(string)
  default = []
}

# https://www.packer.io/plugins/builders/linode
source "linode" "example" {
  image             = "linode/fedora35"
  image_description = "This image was created using Packer."
  image_label       = "packer-fedora-35"
  instance_label    = "temp-packer-fedora-35"
  instance_type     = "g6-nanode-1"
  linode_token      = "${var.linode_token}"
  region            = "us-east"
  ssh_username      = "root"
  authorized_keys   = "${var.sshkeys}"
}

build {
  sources = ["source.linode.example"]

  provisioner "ansible" {
    groups                  = ["containerhosts"]
    inventory_directory     = "./ansible/inventory"
    ansible_env_vars        = ["ANSIBLE_ROLES_PATH=./ansible/roles-local:./ansible/roles-remote"]
    playbook_file           = "./ansible/containerhosts.yml"
    #extra_arguments         = [ "-vvvv" ]
    use_proxy               = false
  }
}

