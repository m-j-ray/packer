#!/bin/bash

# Linode
export PKR_VAR_linode_token=`lpass show --field=token terraform/linode`
export PKR_VAR_linode_root_password=`lpass show --field=root_password terraform/linode`
export PKR_VAR_sshkeys=(`cat ~/.ssh/id_dsa.pub`)
# example packer list var
#    variable "sshkeys" {
#    type = list(string)
#    default = []
#    }