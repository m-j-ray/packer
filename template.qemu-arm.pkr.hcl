# not working 
## https://github.com/solo-io/packer-plugin-arm-image
## https://gitlab.com/qemu-project/qemu/-/issues/45

source "qemu" "name" {
        ssh_username = "username"
        iso_url = "https://cdimage.ubuntu.com/releases/20.04/release/ubuntu-20.04.3-live-server-arm64.iso"
        iso_checksum = "d6fea1f11b4d23b481a48198f51d9b08258a36f6024cb5cec447fe78379959ce"
#        qemu_binary = "qemu-kvm"
        qemu_binary = "qemu-system-arm"
#        qemu_binary = "qemu-system-aarch64"
        machine_type = "virt"
        accelerator = "none"
#        qemu_binary_source_path = "/usr/bin/qemu-arm-static"
#        qemu_binary_destination_path = "/usr/bin/qemu-arm-static"
}

build {
        sources = ["source.qemu.name"]
}
