source "qemu" "f34-amd64" {
  ssh_username = "username"
  iso_url      = "http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/iso/Fedora-Server-dvd-x86_64-34-1.2.iso"
  iso_checksum = "0b9dc87d060c7c4ef89f63db6d4d1597dd3feaf4d635ca051d87f5e8c89e8675"
#  iso_url      = "http://mirrors.kernel.org/fedora/releases/34/Everything/x86_64/iso/Fedora-Everything-netinst-x86_64-34-1.2.iso"
#  iso_checksum = "eb617779a454f9792a84985d1d6763f78c485e89a0d09e9e62b4dabcd540aff1"
#  iso_url      = "http://mirrors.kernel.org/fedora/releases/34/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-34-1.2.iso"
#  iso_checksum = "865c4457dd066d3074c35a8847c8dac1380667c96a4f6d74526324dba14f1b5c"
}



build {
  sources = ["source.qemu.f34-amd64"]
}
