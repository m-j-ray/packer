source "qemu" "f34-amd64" {
  accelerator      = "kvm"
  #   boot_command     = ["<tab> linux inst.text net.ifnames=0 biosdevname=0<enter><wait>"]
  boot_command     = ["<tab> linux inst.text net.ifnames=0 biosdevname=0 inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/kde.ks<enter><wait>"]
  #   boot_command     = ["<tab> linux inst.text net.ifnames=0 biosdevname=0 inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/fedora/34/fedora-live-kde-base.ks<enter><wait>"]
  #   boot_command     = ["<tab> linux inst.text net.ifnames=0 biosdevname=0 inst.ks=cdrom:/fedora-live-kde-base.ks<enter><wait>"]
  boot_wait        = "40s"
  #   disk_cache       = "none"
  disk_compression = true
  #   disk_discard     = "unmap"
  #   disk_interface   = "virtio"
  disk_size        = "40G"
  format           = "qcow2"
  headless         = "false"
  http_directory   = "./http"
  #cd_files   = ["./http/fedora/34/*"]
  #  iso_checksum  = "e1a38b9faa62f793ad4561b308c31f32876cfaaee94457a7a9108aaddaeec406"
  #  iso_url       = "https://download.fedoraproject.org/pub/fedora/linux/releases/34/Server/x86_64/iso/Fedora-Server-netinst-x86_64-34-1.2.iso"
  iso_url          = "http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/iso/Fedora-Server-dvd-x86_64-34-1.2.iso"
  iso_checksum     = "0b9dc87d060c7c4ef89f63db6d4d1597dd3feaf4d635ca051d87f5e8c89e8675"
  #   net_device       = "virtio-net"
  output_directory = "artifacts/qemu/fedora34"
  qemu_binary      = "/usr/bin/qemu-system-x86_64"
  qemuargs         = [["-m", "2048M"], ["-smp", "2"]]
  shutdown_command = "sudo /usr/sbin/shutdown -h now"
  ssh_password     = "testtest"
  #  ssh_private_key_file = "./sshkeys/id_rsa"
  ssh_username     = "user"
  ssh_wait_timeout = "30m"
}

build {
  sources = ["source.qemu.f34-amd64"]

  #  provisioner "shell" {
  #    execute_command = "{{ .Vars }} sudo -E bash '{{ .Path }}'"
  #    inline          = ["dnf -y install ansible"]
  #  }

  #  provisioner "ansible-local" {
  #    playbook_dir  = "ansible"
  #    playbook_file = "ansible/playbook.yml"
  #  }

  #  post-processor "shell-local" {
  #    environment_vars = ["IMAGE_NAME=fedora", "IMAGE_VERSION=34", "DESTINATION_SERVER=download.goffinet.org"]
  #    script           = "scripts/push-image.sh"
  #  }
}
